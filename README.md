# Micron Standard API Response Classes #

This package contains interfaces and base classes to facilitate consistent API responses throughout systems.

See https://api-docs.tmg-secure.com/ => Generic API Responses for documentation and structure.

### Requirements ###

* PHP >= 7.2
* Laravel >= 5.8

### Setup ###

* Include in project **composer.json**


```
#!javascript

    {
        "require": {
            "micron/api-response": "dev-master"
        },
        "repositories": [
            {
                "type": "vcs",
                "url":  "git@bitbucket.org:micronresearch/api-response.git"
            }
        ]
    }
```

* Add provider to /config/app.php 'providers' list:

```
    \Micron\ApiResponse\Providers\ApiResponseServiceProvider::class,
```

 * Update exception handler to extend the handler within this package
 
```
    namespace App\Exceptions;
    class Handler extends \Micron\ApiResponse\Exceptions\Handler
    {
        ...
```

 * Add middleware to 'api' group in app/Http/Kernal.php
 
```
    'api' => [
        ...
        \Micron\ApiResponse\Http\Middleware\ValidateResponseFormat::class,
    ],
```

 * To override language, create a new language file named *api-response* and follow the structure in /src/Resources/lang/en/api-response.php
 
```
return [
    'error' => [
        '400' => [
            'name' => 'Bad Request',
            'message' => 'The request cannot be fulfilled due to bad syntax.'
        ],
    ],
    'success' => [
        '200' => [
            'name' => 'OK',
            'message' => 'Request completed successfully.'
        ],
    ],
];
```

### Usage ###

```
    // Returning a successful response
    return response()->jsonSuccess(...);
    // or
    return new JsonHttpSuccessResponse(...);
```
```
    // Returning an error response
    return response()->jsonError(...);
    // or
    return new JsonHttpErrorResponse(...);
```

### Older Versions: Use 1.* ###

* PHP >= 7.1
* Laravel >= 5.1

```
#!javascript

    {
        "require": {
            "micron/api-response": "1.*"
        },
    }
```