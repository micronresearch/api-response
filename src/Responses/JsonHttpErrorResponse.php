<?php
namespace Micron\ApiResponse\Responses;

class JsonHttpErrorResponse extends BaseJsonHttpResponse
{
    /**
     * Valid Codes
     *
     * @return array
     */
    protected function apiResponseValidCodes()
    {
        // REMEMBER: If codes are added to this list, also update /src/Resources/lang/en/api-response
        return [
            400,401,402,403,404,405,406,408,409,410,411,412,413,414,415,417,419,422,423,426,428,429,
            500,501,502,503,504,505,
        ];
    }

    /**
     * JsonHttpErrorResponse constructor.
     * @param int $httpCode
     * @param null $message
     * @param array $data
     * @param string|null $errorText
     * @param array $headers
     * @param int $options
     */
    public function __construct(
        $httpCode = 500,
        $message = null,
        array $data = [],
        $errorText = null,
        array $headers = [],
        $options = 0
    ) {
        $status = $this->getApiResponseCode($httpCode);
        $this->setStatusCode($status);
        $response = [
            'code' => $status,
            'message' => $this->getApiResponseMessage($status, $message),
            'data' => $this->getApiResponseData($data),
            'error' => $errorText ?: $this->getApiResponseStatusMessage($status),
        ];
        $headers = $this->getApiResponseHeaders($headers);
        parent::__construct($response, $status, $headers, $options);
    }
}
