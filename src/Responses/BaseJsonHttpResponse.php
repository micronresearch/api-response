<?php
namespace Micron\ApiResponse\Responses;

use Illuminate\Http\JsonResponse;

/**
 * Class BaseJsonHttpResponse
 *
 * @package Micron\ApiResponse\Responses
 *
 * @property string $statusListFile
 */
abstract class BaseJsonHttpResponse extends JsonResponse
{
    /**
     * Valid Codes
     *
     * @return array
     */
    abstract protected function apiResponseValidCodes();

    /**
     * Check Code
     *
     * @param int $httpCode
     * @return int
     * @throws \InvalidArgumentException
     */
    protected function getApiResponseCode(int $httpCode)
    {
        if (!in_array($httpCode, $this->apiResponseValidCodes())) {
            throw new \InvalidArgumentException(
                sprintf(
                    '%s is not a valid %s HTTP code. Choose a different response type or update list of codes.',
                    $httpCode,
                    $this->apiResponseClass()
                )
            );
        }
        return $httpCode;
    }

    /**
     * Get Status
     *
     * @param int $httpCode
     * @return string|null
     */
    protected function getApiResponseStatusMessage(int $httpCode)
    {
        return $this->apiResponseText($httpCode, 'name');
    }

    /**
     * Get Message
     *
     * @param int $httpCode
     * @param null|string $message
     * @return string
     */
    protected function getApiResponseMessage(int $httpCode, $message)
    {
        if ($message !== null) {
            return $message;
        }
        return $this->apiResponseTranslate($httpCode, 'message');
    }

    /**
     * Get Data
     *
     * @param array|mixed $data
     * @return array|mixed
     */
    protected function getApiResponseData($data)
    {
        if (!empty($data)) {
            return $data;
        }
        return [];
    }

    /**
     * Get Headers
     *
     * @param array $headers
     * @return array
     */
    protected function getApiResponseHeaders(array $headers)
    {
        return array_merge($this->getApiResponseRequiredHeaders(), $headers);
    }

    /**
     * Get Required Headers
     *
     * @return array
     */
    protected function getApiResponseRequiredHeaders()
    {
        return [
            'Content-Type' => 'application/json',
            'X-Response-Type' => $this->apiResponseClass()
        ];
    }

    /**
     * Text
     *
     * This is the same as translate, but ALWAYS returns English.
     * This means it is not reliant on locale and the value is always consistent.
     * Generally these values should be used in code, not displayed to user, therefore this is most useful for
     * $this->getApiResponseStatusMessage
     *
     * @param $httpCode
     * @param $key
     * @param null $responseType
     * @return mixed
     */
    protected function apiResponseText(int $httpCode, $key, $responseType = null)
    {
        $responseType = $responseType?$responseType:$this->getApiResponseType();
        $key = 'api-response.'.$responseType.'.'.$httpCode.'.'.$key;
        return \Lang::has($key, 'en', false)?\Lang::get($key, [], 'en', false)
            :(\Lang::has('Micron\ApiResponse::'.$key, 'en', false)?
                \Lang::get('Micron\ApiResponse::'.$key, [], 'en', false)
                :($httpCode.' '.ucfirst($responseType)));
    }

    /**
     * Translate
     *
     * @param $httpCode
     * @param $key
     * @param null $responseType
     * @return mixed
     */
    protected function apiResponseTranslate(int $httpCode, $key, $responseType = null)
    {
        $responseType = $responseType?$responseType:$this->getApiResponseType();
        $key = 'api-response.'.$responseType.'.'.$httpCode.'.'.$key;
        return \Lang::has($key)?\Lang::get($key)
            :(\Lang::has('Micron\ApiResponse::'.$key)?\Lang::get('Micron\ApiResponse::'.$key)
                :($httpCode.' '.ucfirst($responseType)));
    }

    /**
     * Response Type
     *
     * @return string : 'error' or 'success'
     */
    protected function getApiResponseType()
    {
        return $this->isSuccessful()?'success':'error';
    }

    /**
     * Response Class
     *
     * @return string
     */
    protected function apiResponseClass()
    {
        try {
            return (new \ReflectionClass($this))->getShortName();
        } catch (\ReflectionException $e) {
            return basename(__CLASS__);
        }
    }
}
