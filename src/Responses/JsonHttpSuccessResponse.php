<?php
namespace Micron\ApiResponse\Responses;

class JsonHttpSuccessResponse extends BaseJsonHttpResponse
{
    /**
     * Valid Codes
     *
     * @return array
     */
    protected function apiResponseValidCodes()
    {
        // REMEMBER: If codes are added to this list, also update /src/Resources/lang/en/api-response
        return [200];
    }

    /**
     * JsonHttpSuccessResponse constructor.
     *
     * @param array $data
     * @param int $httpCode
     * @param null $message
     * @param string|null $statusText
     * @param array $headers
     * @param int $options
     */
    public function __construct(
        array $data = [],
        $httpCode = 200,
        $message = null,
        $statusText = null,
        $headers = [],
        $options = 0
    ) {
        $status = $this->getApiResponseCode($httpCode);
        $this->setStatusCode($status);
        $response = [
            'code' => $status,
            'message' => $this->getApiResponseMessage($status, $message),
            'data' => $this->getApiResponseData($data),
            'status' => $statusText ?: $this->getApiResponseStatusMessage($status),
        ];
        $headers = $this->getApiResponseHeaders($headers);
        parent::__construct($response, $status, $headers, $options);
    }
}
