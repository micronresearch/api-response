<?php
namespace Micron\ApiResponse\Providers;

use Illuminate\Support\ServiceProvider;
use Micron\ApiResponse\Http\Middleware\ValidateResponseFormat;
use Micron\ApiResponse\Contracts\Middleware\ValidateResponseFormat as ValidateResponseFormatContract;
use Micron\ApiResponse\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory as ViewFactoryContract;
use Illuminate\Contracts\Routing\ResponseFactory as ResponseFactoryContract;

/**
 * Class ConfigServiceProvider
 *
 * @package App\Providers
 */
class ApiResponseServiceProvider extends ServiceProvider
{
    /**
     * Load config from database on boot
     *
     * @return void
     */
    public function boot()
    {
        /* Load default translations */
        \Lang::addNamespace('Micron\ApiResponse', __DIR__.'/../Resources/lang');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /* Register updated response factory */
        $this->app->singleton(ResponseFactoryContract::class, function ($app) {
            return new ResponseFactory($app[ViewFactoryContract::class], $app['redirect']);
        });
        /* Bind middleware class */
        $this->app->bind(ValidateResponseFormatContract::class, ValidateResponseFormat::class);
    }
}
