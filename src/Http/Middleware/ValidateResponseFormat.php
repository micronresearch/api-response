<?php
namespace Micron\ApiResponse\Http\Middleware;

use Closure;
use Micron\ApiResponse\Contracts\Middleware\ValidateResponseFormat as ValidateResponseFormatContract;
use Micron\ApiResponse\Responses\JsonHttpErrorResponse;
use Micron\ApiResponse\Responses\JsonHttpSuccessResponse;

/**
 * Class ValidateResponseFormat
 *
 * @package App\Http\Middleware
 */
class ValidateResponseFormat implements ValidateResponseFormatContract
{
    /**
     * Handle an incoming request.
     *
     * Just skip if !expectsJson
     *
     * Checks that X-Response-Type header is set, if it doesn't then the response has not been formatted to common
     * format, wrap in JsonSuccessResponse or JsonErrorResponse
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request); /** @var \Illuminate\Http\Response $response */
        if ($request->expectsJson()) {
            $response = $this->reFormatResponse($response);
        }
        return $response;
    }

    /**
     * Re-Format Response
     *
     * @param \Illuminate\Http\Response $response
     * @return \Illuminate\Http\Response|JsonHttpSuccessResponse|JsonHttpErrorResponse
     * @throws \Exception
     */
    public function reFormatResponse($response)
    {
        if ($response->headers->has('X-Response-Type')) {
            return $response; /* Already in correct format */
        }
        $content = json_decode($response->getContent(), 1);
        if (json_last_error() !== JSON_ERROR_NONE || !is_array($content)) {
            $content = $response->getContent();
        }
        if (!is_array($content)) {
            $message = $content;
            $data = [$content];
        } else {
            $message = null;
            if (!empty($content['message'])) { // 'message' will override 'error'
                $message = $content['message'];
            } elseif (!empty($content['error'])) {
                $message = $content['error'];
            }
            $data = $content;
        }
        if ($response->isSuccessful()) {
            return response()->jsonSuccess(
                $data,
                $response->getStatusCode(),
                $message,
                null,
                $response->headers->all()
            );
        } else {
            return response()->jsonError(
                $response->getStatusCode(),
                $message,
                $data,
                null,
                $response->headers->all()
            );
        }
    }
}
