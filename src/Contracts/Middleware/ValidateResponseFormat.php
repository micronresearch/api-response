<?php
namespace Micron\ApiResponse\Contracts\Middleware;

use Closure;

/**
 * Class ValidateResponseFormat
 */
interface ValidateResponseFormat
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next);

    /**
     * Re-Format Response
     *
     * @param \Illuminate\Http\Response $response
     * @return \Illuminate\Http\Response
     */
    public function reFormatResponse($response);
}
