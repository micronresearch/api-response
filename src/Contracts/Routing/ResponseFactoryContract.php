<?php
namespace Micron\ApiResponse\Contracts\Routing;

use Illuminate\Contracts\Routing\ResponseFactory;
use Micron\ApiResponse\Responses\JsonHttpErrorResponse;
use Micron\ApiResponse\Responses\JsonHttpSuccessResponse;

interface ResponseFactoryContract extends ResponseFactory
{
    /**
     * Return a successful formatted JSON response from the application.
     *
     * @param  array $data
     * @param int $httpCode
     * @param string|null $message
     * @param string|null $statusText
     * @param  array $headers
     * @param  int $options
     * @return \Illuminate\Http\JsonResponse|JsonHttpSuccessResponse
     */
    public function jsonSuccess(
        array $data = [],
        $httpCode = 200,
        $message = null,
        $statusText = null,
        array $headers = [],
        $options = 0
    );

    /**
     * Return an error formatted JSON response from the application.
     *
     * @param int $httpCode
     * @param string|null $message
     * @param  array $data
     * @param  string|null $errorText
     * @param  array $headers
     * @param  int $options
     * @return \Illuminate\Http\JsonResponse|JsonHttpErrorResponse
     */
    public function jsonError(
        $httpCode = 500,
        $message = null,
        array $data = [],
        $errorText = null,
        array $headers = [],
        $options = 0
    );
}
