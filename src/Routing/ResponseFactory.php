<?php
namespace Micron\ApiResponse\Routing;

use Micron\ApiResponse\Contracts\Routing\ResponseFactoryContract;
use Micron\ApiResponse\Responses\JsonHttpErrorResponse;
use Micron\ApiResponse\Responses\JsonHttpSuccessResponse;

class ResponseFactory extends \Illuminate\Routing\ResponseFactory implements ResponseFactoryContract
{
    /**
     * Return a successful formatted JSON response from the application.
     *
     * @param  string|array $data
     * @param int $httpCode
     * @param null|string $message
     * @param null|string $statusText
     * @param  array $headers
     * @param  int $options
     * @return \Illuminate\Http\JsonResponse|JsonHttpSuccessResponse
     * @throws \Exception
     */
    public function jsonSuccess(
        array $data = [],
        $httpCode = 200,
        $message = null,
        $statusText = null,
        array $headers = [],
        $options = 0
    ) {
        return new JsonHttpSuccessResponse($data, $httpCode, $message, $statusText, $headers, $options);
    }

    /**
     * Return an error formatted JSON response from the application.
     *
     * @param int $httpCode
     * @param null|string $message
     * @param  string|array $data
     * @param  string|null $errorText
     * @param  array $headers
     * @param  int $options
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function jsonError(
        $httpCode = 500,
        $message = null,
        array $data = [],
        $errorText = null,
        array $headers = [],
        $options = 0
    ) {
        return new JsonHttpErrorResponse($httpCode, $message, $data, $errorText, $headers, $options);
    }
}
