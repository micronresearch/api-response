<?php

namespace Micron\ApiResponse\Exceptions;

use Micron\ApiResponse\Http\Middleware\ValidateResponseFormat;
use Exception;

class Handler extends \Illuminate\Foundation\Exceptions\Handler
{
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, \Throwable $exception)
    {
        $response = parent::render($request, $exception);
        if ($request->expectsJson()) {
            $response = app()->make(ValidateResponseFormat::class)->reFormatResponse($response);
        }
        return $response;
    }
}
